
const express = require('express')
const app = express();
const mongo = require('mongodb').MongoClient
const url = "mongodb+srv://Dan_Marshall:Schoolsucks1!@dandb-ujy98.mongodb.net/test";
var cors = require("cors");

// REQUESTS
const bodyParser = require('body-parser');
const port = process.env.PORT || 5000;
let returnedItems;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

app.post('/api/lists', (req, res) => {
  mongo.connect(url, (err, client) => {
    if (err) {
      console.error(err)
      return
    }

    const db = client.db('warhammer');
    const collection = db.collection('lists');
    collection.find({})
      .toArray((err, items) => {
        sendResponse(items, res)
      })
    client.close()
  })
});

app.post('/api/addList', (req, res) => {
  let listname = req.body.listName;
  let faction = req.body.faction;
  let subfaction = req.body.subfaction;
  let points  = req.body.points;
  let entries  = [];
  
  mongo.connect(url, (err, client) => {
    if (err) {
      console.error(err)
      return
    }
    const db = client.db('warhammer');
    const collection = db.collection('lists');
    collection.insertOne(
      {
      "name": listname,
      "army": faction,
      "subfaction": subfaction,
      "points": points,
      "entries": entries
      }
    , function (error, response) {
      respObj = {
        worked:true
      }
      if(error) {
          console.log('Error occurred while inserting');
      } else {
        res.send(respObj);
         client.close()
      }
    });
  })
})

app.post('/api/updateList', (req, res) => {
  let list = req.body.list;
  let data = req.body.data;
  mongo.connect(url, (err, client) => {
    if (err) {
      console.error(err)
      return
    }

    const db = client.db('warhammer');
    const collection = db.collection('lists');
    collection.updateOne(
      { name: list },
      { $set: { entries : data } },
      { upsert: true }
      )

    client.close()
  })
});

app.post('/api/deleteList', (req, res) => {
  let list = req.body.list;
  
  mongo.connect(url, (err, client) => {
    if (err) {
      console.error(err)
      return
    }
    const db = client.db('warhammer');
    const collection = db.collection('lists');
    collection.deleteOne(
      {"name": list}
    , function (error, response) {
      if(error) {
          console.log('Error occurred while inserting');
      } else {
         client.close()
      }
    });
  })
})

sendResponse = (items, res) => {
  res.send(items);
}

app.listen(port, () => console.log(`Listening on port ${port}`));