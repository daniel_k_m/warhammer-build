/*eslint-disable*/
import React, { Component } from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classnames from "classnames";

import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

class FixedPlugin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: "dropdown show",
      bg_checked: true,
      bgImage: this.props.bgImage
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.handleFixedClick();
  }

  handleInput = () => {
    
  }

  addNewList = () => {
    let listName = localStorage.getItem("listName");
    if(listName.length > 2) {
      fetch('http://localhost:5000/api/addList', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({ post: listName})})
      .then(response => {
          this.setState({ 
              responseToPost: response 
          });
      })
    }
  }

  render() {
    return (
      <div
        className={classnames("fixed-plugin", {
          "rtl-fixed-plugin": this.props.rtlActive
        })}
      >
        <div id="fixedPluginClasses" className={this.props.fixedClasses} >
          <div style={{color:"white", cursor: "pointer"}} onClick={this.handleClick}>
            <p>Add List</p>
            <i className="fa" />
          </div>
          <ul className="dropdown-menu">
            <li className="header-title">Themes</li>
            <li className="adjustments-line">
              <a className="switch-trigger">
                <div>
                  <span
                    className={
                      this.props.bgColor === "purple"
                        ? "badge filter badge-purple active"
                        : "badge filter badge-purple"
                    }
                    data-color="purple"
                    onClick={() => {
                      this.props.handleColorClick("purple");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "blue"
                        ? "badge filter badge-blue active"
                        : "badge filter badge-blue"
                    }
                    data-color="blue"
                    onClick={() => {
                      this.props.handleColorClick("blue");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "green"
                        ? "badge filter badge-green active"
                        : "badge filter badge-green"
                    }
                    data-color="green"
                    onClick={() => {
                      this.props.handleColorClick("green");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "red"
                        ? "badge filter badge-red active"
                        : "badge filter badge-red"
                    }
                    data-color="red"
                    onClick={() => {
                      this.props.handleColorClick("red");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "orange"
                        ? "badge filter badge-orange active"
                        : "badge filter badge-orange"
                    }
                    data-color="orange"
                    onClick={() => {
                      this.props.handleColorClick("orange");
                    }}
                  />
                </div>
              </a>
            </li>
              <li className="button-container">
              <div className="button-container">
                  <CustomInput
                    labelText="Enter list name"
                    id="listname"
                    inputProps={this.handleInput}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
              </div>
            </li>
            <li className="button-container">
              <div className="button-container">
                <Button
                  style={{marginTop:"30%"}}
                  color="success"
                  target="_blank"
                  fullWidth
                  onClick={this.addNewList}
                >
                  Add New List
                </Button>
              </div>
            </li>
            <li className="button-container">
            </li>
            <li className="adjustments-line" />
          </ul>
        </div>
      </div>
    );
  }
}

FixedPlugin.propTypes = {
  bgImage: PropTypes.string,
  handleFixedClick: PropTypes.func,
  rtlActive: PropTypes.bool,
  fixedClasses: PropTypes.string,
  bgColor: PropTypes.oneOf(["purple", "blue", "green", "orange", "red"]),
  handleColorClick: PropTypes.func,
  handleImageClick: PropTypes.func
};

export default FixedPlugin;
