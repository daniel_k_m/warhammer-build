/*eslint-disable*/
import React, { Component } from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classnames from "classnames";

import warhammer1 from "assets/img/tau.jpg";
import warhammer2 from "assets/img/tyranid.jpg";
import warhammer3 from "assets/img/daemon.jpg";
import warhammer4 from "assets/img/csm.jpg";
import warhammer5 from "assets/img/halo.jpg";
import warhammer6 from "assets/img/ork.jpg";
import warhammer7 from "assets/img/dg.jpg";
import warhammer8 from "assets/img/sw.jpg";

import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

class FixedPlugin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: "dropdown show",
      bg_checked: true,
      bgImage: this.props.bgImage
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.handleFixedClick();
  }

  handleInput = () => {
    
  }

  addNewList = () => {
    let listName = localStorage.getItem("listName");
    let faction = localStorage.getItem("faction");
    let subfaction = localStorage.getItem("subfaction");
    let points = localStorage.getItem("pointlevel");
    if(listName != null && faction != null && subfaction != null && points != null) {
      if(listName.length > 2 && listName !== "  " && faction.length > 2 && faction !== "  " && subfaction.length > 2 && subfaction !== "  " && points.length > 2 && points !== "  ") {
        fetch('http://localhost:5000/api/addList', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(
          { listName: listName,
            faction: faction,
            subfaction: subfaction,
            points: points
          })
        })
        .then(response => {
            this.setState({ 
                responseToPost: response 
            });
            window.location.reload()
        })
      }
    } else {
      alert("Please fill in some of them blanks!")
    }
  }

  render() {
    return (
      <div
        className={classnames("fixed-plugin", {
          "rtl-fixed-plugin": this.props.rtlActive
        })}
      >
        <div id="fixedPluginClasses" className={this.props.fixedClasses} >
          <div style={{color:"white", cursor: "pointer"}} onClick={this.handleClick}>
            <p>Add List</p>
            <i className="fa" />
          </div>
          <ul className="dropdown-menu" style={{height: "900px", border: "1px solid grey"}}>
            <li className="header-title">Themes</li>
            <li className="adjustments-line">
              <a className="switch-trigger">
                <div>
                  <span
                    className={
                      this.props.bgColor === "purple"
                        ? "badge filter badge-purple active"
                        : "badge filter badge-purple"
                    }
                    data-color="purple"
                    onClick={() => {
                      this.props.handleColorClick("purple");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "blue"
                        ? "badge filter badge-blue active"
                        : "badge filter badge-blue"
                    }
                    data-color="blue"
                    onClick={() => {
                      this.props.handleColorClick("blue");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "green"
                        ? "badge filter badge-green active"
                        : "badge filter badge-green"
                    }
                    data-color="green"
                    onClick={() => {
                      this.props.handleColorClick("green");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "red"
                        ? "badge filter badge-red active"
                        : "badge filter badge-red"
                    }
                    data-color="red"
                    onClick={() => {
                      this.props.handleColorClick("red");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "orange"
                        ? "badge filter badge-orange active"
                        : "badge filter badge-orange"
                    }
                    data-color="orange"
                    onClick={() => {
                      this.props.handleColorClick("orange");
                    }}
                  />
                </div>
              </a>
            </li>
            <li className="header-title">Images</li>
            <li className={this.state["bgImage"] === warhammer1 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer1 });
                  this.props.handleImageClick(warhammer1);
                }}
              >
                <img src={warhammer1} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer2 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer2 });
                  this.props.handleImageClick(warhammer2);
                }}
              >
                <img src={warhammer2} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer3 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer3 });
                  this.props.handleImageClick(warhammer3);
                }}
              >
                <img src={warhammer3} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer4 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer4 });
                  this.props.handleImageClick(warhammer4);
                }}
              >
                <img src={warhammer4} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer5 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer5 });
                  this.props.handleImageClick(warhammer5);
                }}
              >
                <img src={warhammer5} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer6 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer6 });
                  this.props.handleImageClick(warhammer6);
                }}
              >
                <img src={warhammer6} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer7 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer7 });
                  this.props.handleImageClick(warhammer7);
                }}
              >
                <img src={warhammer7} alt="..." />
              </a>
            </li>
            <li className={this.state["bgImage"] === warhammer8 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({ bgImage: warhammer8 });
                  this.props.handleImageClick(warhammer8);
                }}
              >
                <img src={warhammer8} alt="..." />
              </a>
            </li>
            <li className="button-container">
              <div className="button-container">
                  <CustomInput
                    labelText="Enter list name"
                    id="listname"
                    inputProps={this.handleInput}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  <CustomInput
                    labelText="Enter faction"
                    id="faction"
                    inputProps={this.handleInput}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  <CustomInput
                    labelText="Enter subfaction"
                    id="subfaction"
                    inputProps={this.handleInput}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  <CustomInput
                    labelText="Enter point level"
                    id="pointlevel"
                    inputProps={this.handleInput}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
              </div>
            </li>
            <li className="button-container">
              <div className="button-container">
                <Button
                  style={{marginTop:"120%"}}
                  color="success"
                  target="_blank"
                  fullWidth
                  onClick={this.addNewList}
                >
                  Add New List
                </Button>
              </div>
            </li>
            <li className="button-container">
            </li>
            <li className="adjustments-line" />
          </ul>
        </div>
      </div>
    );
  }
}

FixedPlugin.propTypes = {
  bgImage: PropTypes.string,
  handleFixedClick: PropTypes.func,
  rtlActive: PropTypes.bool,
  fixedClasses: PropTypes.string,
  bgColor: PropTypes.oneOf(["purple", "blue", "green", "orange", "red"]),
  handleColorClick: PropTypes.func,
  handleImageClick: PropTypes.func
};

export default FixedPlugin;
