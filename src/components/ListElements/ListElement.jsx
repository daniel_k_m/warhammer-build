import React from "react";
// nodejs library to set properties for components
// import PropTypes from "prop-types";
// import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
// import MenuItem from "@material-ui/core/MenuItem";
// import MenuList from "@material-ui/core/MenuList";
// import Grow from "@material-ui/core/Grow";
// import Paper from "@material-ui/core/Paper";
// import ClickAwayListener from "@material-ui/core/ClickAwayListener";
// import Hidden from "@material-ui/core/Hidden";
// import Poppers from "@material-ui/core/Popper";
// import Divider from "@material-ui/core/Divider";
// // @material-ui/icons
// import Person from "@material-ui/icons/Person";
// import Notifications from "@material-ui/icons/Notifications";
// import Dashboard from "@material-ui/icons/Dashboard";
// import Search from "@material-ui/icons/Search";
// // core components
// import CustomInput from "components/CustomInput/CustomInput.jsx";
// import Button from "components/CustomButtons/Button.jsx";
import GridItem from "components/Grid/GridItem.jsx";
// import GridContainer from "components/Grid/GridContainer.jsx";
// import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import MaterialTable from 'material-table';

import headerLinksStyle from "assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";


class ListElement extends React.Component {
  state = {
    openNotifcation: false,
    openProfile: false,
    columns: [
      { title: 'Unit', field: 'unit' },
      { title: 'Amount', field: 'amount', type: 'numeric' },
      { title: 'Type', field: 'type' },
      { title: 'Equipment', field: 'equipment' },
      { title: 'Abilities', field: 'abilities' },
      { title: 'Points', field: 'points', type: 'numeric' },
      { title: 'Power Level', field: 'powerLevel', type: 'numeric' }
    ],
    data: [
      { unit: '', amount: 0, type: '', equipment: "", abilities: "", points: 0, powerLevel: 0  }
    ]
  }    

  componentWillReceiveProps(nextProps) {
    this.setState({data: nextProps.entries})
  }

  toggleTable = (e) => {
    let table = e.currentTarget.parentElement.children[1];
    if (table.style.display === "block") {
      table.style.display = "none";
    } else {
      table.style.display = "block";
    }
  }

  handleToggleNotification = () => {
    this.setState(state => ({ openNotifcation: !state.openNotifcation }));
  };

  handleCloseNotification = event => {
    if (this.anchorNotification.contains(event.target)) {
      return;
    }
    this.setState({ openNotifcation: false });
  };

  handleToggleProfile = () => {
    this.setState(state => ({ openProfile: !state.openProfile }));
  };

  handleCloseProfile = event => {
    if (this.anchorProfile.contains(event.target)) {
      return;
    }
    this.setState({ openProfile: false });
  };

  updateDatabase = (list, data) => {
    this.setState({loadingLists: true});
      fetch('http://localhost:5000/api/updateList', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({ list: list, data: data})})
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        
      })
  }

  deleteList = (e) => {
    let currentTable = e.currentTarget.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let currentList = e.currentTarget.parentElement.children[1].innerHTML;
    if(window.confirm("Are You sure you want to delete this list permanently?")){
      currentTable.style.display = "none";
    }
    fetch('http://localhost:5000/api/deleteList', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({ list: currentList})})
    .then(response => {
      return response.json();
    })
    .then(myJson => {

    })
  }

  render() {
    const { classes } = this.props;
    const list = this.props.name;
    const title = 
          <div style={{padding: '0px 10px'}}>
            <Button color="primary" round onClick={(e) => this.deleteList(e)} style={{marginRight: "20px"}}>
              Delete List
            </Button>
            <div style={{float:"right"}}>{this.props.name}</div>
          </div>;

    return (
          <GridItem xs={12} sm={12} md={12}>
            <Card >
              <CardHeader color="primary" style={{cursor:'pointer'}} onClick={(e) => {this.toggleTable(e)}}>
                <h4 className={classes.cardTitleWhite}>{this.props.name} - {this.props.army} - {this.props.subfaction} - {this.props.points}</h4>
              </CardHeader>
              <CardBody style={{display:"none"}}>
                <MaterialTable
                  title={title}
                  columns={this.state.columns}
                  data={this.state.data}
                  editable={{
                    onRowAdd: newData =>
                      new Promise(resolve => {
                        setTimeout(() => {
                          resolve();
                          let data = [...this.state.data];  
                          if(data === undefined) {
                            data = [];
                          }          
                          data.push(newData);
                          this.updateDatabase(list, data)
                          this.setState({ ...this.state, data });
                        }, 600);
                      }),
                    onRowUpdate: (newData, oldData) =>
                      new Promise(resolve => {
                        setTimeout(() => {
                          resolve();
                          let data = [...this.state.data];
                          data[data.indexOf(oldData)] = newData;
                          this.updateDatabase(list, data)
                          this.setState({ ...this.state.data, data });
                        }, 600);
                      }),
                    onRowDelete: oldData =>
                      new Promise(resolve => {
                        setTimeout(() => {
                          resolve();
                          let data = [...this.state.data];
                          data.splice(data.indexOf(oldData), 1);
                          this.updateDatabase(list, data)
                          this.setState({ ...this.state, data });
                        }, 600);
                      }),
                  }}        
                  actions={[
                    {
                      icon: 'file_copy',
                      tooltip: 'Duplicate Entry',
                      onClick: (event, rowData) => {
                        console.log(rowData)
                        let newData = {
                          unit: rowData.unit,
                          amount: rowData.amount,
                          type: rowData.type,
                          equipment: rowData.equipment,
                          abilities: rowData.abilities,
                          points: rowData.points,
                          powerLevel: rowData.powerLevel
                        }
                        new Promise(resolve => {
                          setTimeout(() => {
                            resolve();
                            let data = [...this.state.data];  
                            if(data === undefined) {
                              data = [];
                            }          

                            data.push(newData);
                            this.updateDatabase(list, data)
                            this.setState({ ...this.state, data });
                          }, 600);
                        })
                      }
                    }
                  ]}
                  // components={{
                  //   Toolbar: props => (
                  //     <div>
                  //       <div style={{padding: '0px 10px'}}>
                  //         <Button color="primary" round onClick={(e) => this.deleteList(e)}>
                  //           Delete List
                  //         </Button>
                  //       </div>
                  //     </div>
                  //   ),
                  // }}
                />
              </CardBody>
            </Card>
          </GridItem>
    );
  }
}

export default withStyles(headerLinksStyle)(ListElement);
