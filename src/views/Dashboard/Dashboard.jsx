import React from "react";
// import axios from "axios";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import { bugs, website, server, chat } from "variables/general.jsx";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

const CSMPoints = 2834;
const CDPoints = 2193;
const deathGuardPoints = 1184;
const miscPoints = 736;

class Dashboard extends React.Component {
  state = {
    value: 0,
    loadedLists: [
      ["","","","",""]
    ],
    tableHeaders: ["Name", "Army", "Subfaction", "Points", "Win/Loss Ratio"]
  };

  componentWillMount() {
    let points = CSMPoints + CDPoints + deathGuardPoints + miscPoints;
    this.setState({ totalPoints: points })
    this.loadLists()
  }

  componentWillReceiveProps() {
    let points = CSMPoints + CDPoints + deathGuardPoints + miscPoints;
    this.setState({ totalPoints: points })
    this.loadLists()
  }

  loadLists = () => {
    let user = "Dan Marshall";
    this.setState({loadingLists: true});
      fetch('http://localhost:5000/api/lists', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({ post: user})})
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        this.setState({loadingLists: false});
        let listArray = [];
        for(let i=0; i<myJson.length; i++) {
          let listItem = [];
          listItem[0] = myJson[i].name;
          listItem[1] = myJson[i].army;
          listItem[2] = myJson[i].subfaction;
          listItem[3] = myJson[i].points;
          listItem[4] = myJson[i].wlRatio;
          // listItem[5] = myJson[i].entries;
          listArray.push(listItem);
        }
        localStorage.setItem("loadedLists", JSON.stringify(listArray));
        this.setState({
          loadedLists: listArray,
          loadingLists: false
        });
      })
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleTableClick = (event) => {
    let e = event.target;
    if(e.tagName !== "TH") {
      let armyListRow = e.parentElement;
      let allRows = e.parentElement.parentElement;
      for(let i=0; i<allRows.childElementCount; i++) {
        allRows.children[i].style.backgroundColor = "white";
        allRows.children[i].style.color = "black";
      }
      armyListRow.style.backgroundColor = "Tan";
      armyListRow.style.color = "white";
      let armyListName = e.parentElement.children[0].innerHTML;
      localStorage.setItem("armyList", armyListName)
      this.setState({armyListName: armyListName})
    }
  };

  handleChangeIndex = (index) => {
    this.setState({ value: index });
  };

  render() {
    const { classes } = this.props;
    const totalPoints = this.state.totalPoints;
    let data = this.state.loadedLists;
    let headers = this.state.tableHeaders;

    return (
      <div>
        <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
            <Card>
              <CardHeader color="warning"><a href="/admin/table" style={{color:"white"}}>
                <h4 className={classes.cardTitleWhite}>Lists</h4>
                <p className={classes.cardCategoryWhite}>
                  New suggested lists available!
                </p></a>
              </CardHeader>
              <CardBody onClick={(event) => this.handleTableClick(event)}>
                {/* <a href="/admin/table"> */}
                  <Table
                    tableHeaderColor="warning"
                    tableHead={headers}
                    tableData={data}
                  />
                {/* </a> */}
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <CustomTabs
              title="Tasks:"
              headerColor="primary"
              tabs={[
                {
                  tabName: "List",
                  tabIcon: Cloud,
                  tabContent: (
                    <Tasks
                      checkedIndexes={[1]}
                      tasksIndexes={[0, 1, 2]}
                      tasks={server}
                    />
                  )
                },
                {
                  tabName: "Rules",
                  tabIcon: BugReport,
                  tabContent: (
                    <Tasks
                      checkedIndexes={[0, 3]}
                      tasksIndexes={[0, 1, 2, 3]}
                      tasks={bugs}
                    />
                  )
                },
                {
                  tabName: "Chat",
                  tabIcon: Code,
                  tabContent: (
                    <Tasks
                      checkedIndexes={[0]}
                      tasksIndexes={[0, 1]}
                      tasks={chat}
                    />
                  )
                },
                {
                  tabName: "Website",
                  tabIcon: Code,
                  tabContent: (
                    <Tasks
                      checkedIndexes={[0]}
                      tasksIndexes={[0, 1]}
                      tasks={website}
                    />
                  )
                }
              ]}
            />
          </GridItem>
          <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="warning">
                <ChartistGraph
                  className="ct-chart"
                  data={dailySalesChart.data}
                  type="Line"
                  options={dailySalesChart.options}
                  listener={dailySalesChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>List Statistics</h4>
                <p className={classes.cardCategory}>
                  <span className={classes.successText}>
                    <ArrowUpward className={classes.upArrowCardCategory} /> 55%
                  </span>{" "}
                  increased success using <b>Berzerkers</b>.
                </p>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated 4 minutes ago
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="danger">
                <ChartistGraph
                  className="ct-chart"
                  data={completedTasksChart.data}
                  type="Line"
                  options={completedTasksChart.options}
                  listener={completedTasksChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Schedule</h4>
                <p className={classes.cardCategory}>
                  Games scheduled and hobby time
                </p>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> campaign sent 2 days ago
                </div>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="primary">
                <ChartistGraph
                  className="ct-chart"
                  data={emailsSubscriptionChart.data}
                  type="Bar"
                  options={emailsSubscriptionChart.options}
                  responsiveOptions={emailsSubscriptionChart.responsiveOptions}
                  listener={emailsSubscriptionChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Feature rollouts</h4>
                <p className={classes.cardCategory}>
                  3 <b>features</b> added and 4 <b>bugs</b> fixed in last <b>two weeks</b>.
                </p>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> campaign sent 2 days ago
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
          <GridItem xs={12} sm={12} md={12} style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
            <h4><b>My Armies - {totalPoints} Points</b></h4>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <Icon>content_copy</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Chaos Space Marines</p>
                <h3 className={classes.cardTitle}>
                  {CSMPoints} <small>Points</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <Danger>
                    <Warning />
                  </Danger>
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    Get more space
                  </a>
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="danger" stats icon>
                <CardIcon color="danger">
                  <Store />
                </CardIcon>
                <p className={classes.cardCategory}>Chaos Daemons</p>
                <h3 className={classes.cardTitle}>
                  {CDPoints} <small>Points</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <DateRange />
                  Last 24 Hours
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <Icon>info_outline</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Death Guard</p>
                <h3 className={classes.cardTitle}>
                  {deathGuardPoints} <small>Points</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <LocalOffer />
                  Tracked from Github
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <Accessibility />
                </CardIcon>
                <p className={classes.cardCategory}>Misc.</p>
                <h3 className={classes.cardTitle}>
                  {miscPoints} <small>Points</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <Update />
                  Just Updated
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        
        <GridContainer>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
