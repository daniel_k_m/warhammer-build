import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
// import Table from "components/Table/Table.jsx";
// import Card from "components/Card/Card.jsx";
// import CardHeader from "components/Card/CardHeader.jsx";
// import CardBody from "components/Card/CardBody.jsx";
import ListElement from "components/ListElements/ListElement.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

class TableList extends React.Component {

  componentWillMount() {
    const { classes } = this.props;
    this.setState({classes: classes})
    this.loadLists(classes)
  }

  componentWillReceiveProps() {
    const { classes } = this.props;
    this.setState({classes: classes})
    this.loadLists(classes)
  }

  loadLists = (classes) => {
    let user = "Dan Marshall";
    this.setState({loadingLists: true});
      fetch('http://localhost:5000/api/lists', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({ post: user})})
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        this.setState({loadingLists: false});
        let listArray = [];
        for(let i=0; i<myJson.length; i++) {
          let listItem = [];
          listItem[0] = myJson[i].name;
          listItem[1] = myJson[i].army;
          listItem[2] = myJson[i].subfaction;
          listItem[3] = myJson[i].points;
          listItem[4] = myJson[i].wlRatio;
          listItem[5] = myJson[i].entries;
          listArray.push(listItem);
        }
        localStorage.setItem("loadedLists", JSON.stringify(listArray));
        this.setState({
          loadedLists: listArray,
          listLength: myJson.length
        });
        this.setState({loadingLists: false});
      })
  }

  listItem = (classes, name, army, subfaction, points, entries) => {
    // let number = Math.random()
    return <ListElement name={name} army={army} subfaction={subfaction} points={points} entries={entries} />
  }

  fullTable = (classes) => {
    let data = this.state.loadedLists;
    let table = [];
    for (let i=0; i<this.state.listLength; i++) {
      let name = data[i][0];
      let army = data[i][1];
      let subfaction = data[i][2];
      let points = data[i][3];
      let entries = data[i][5];
      table.push( this.listItem(classes, name, army, subfaction, points, entries))
     }
    return table
  }

  render() {
    let classes = this.state.classes;
    let number = Math.random()
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>     
          <CustomInput
            labelText="Enter list name"
            key={number.toString()}
            id="tableList"
            inputProps={this.handleInput}
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>     
          { this.fullTable(classes) }
        </GridItem>
      </GridContainer>
    );
  }
  }
  

TableList.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(TableList);
