var bugs = [
  "Charge",
  "Pile in",
  "Consolidate",
  "Deep Strike"
];
var website = [
  "Bug fix - Margin on top navbar doesn't line up",
  'Feature request - Add random number generator to dashboard"'
];
var server = [
  "Had issues with heavy bolters from tanks",
  "Messed up my charge at 7 - 9 inches",
  'Got tabled in two turns - need more survivable units"'
];
var chat = [
  "Hey, free for a game on Saturday?",
  "Nope, got family things"
];

module.exports = {
  // these 3 are used to create the tasks lists in TasksCard - Dashboard view
  bugs,
  website,
  server,
  chat
};
